# -*- coding: utf-8 -*-
from django.template import Library
from urllib import urlencode
register = Library()


@register.assignment_tag(takes_context=True)
def insert_canonical_link(context):
    request = context.get('request')
    canonical_path = "".join(['http://', request.get_host(), request.path])
    params = request.GET
    if params:
        if "start" not in params:
            return None
        else:
            result = ""
            params_copy = params.copy()
            del params_copy["start"]
            if params_copy:
                result += "?" + urlencode(params_copy)
            return "".join([canonical_path, result])
    return None


@register.assignment_tag(takes_context=True)
def insert_canonical_link_product_list_pages(context):
    request = context.get('request')
    canonical_path = ''.join(['http://', request.get_host(), request.path])
    params = request.GET.copy()
    pagination = context.get('pagination')
    if pagination['has_prev']:
        result = ''
        if 'start' in params:
            del params['start']
        if params:
            result += '?' + urlencode(params)
        return ''.join([canonical_path, result])
    return None
