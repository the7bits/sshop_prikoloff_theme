from distutils.core import setup
from setuptools import find_packages
setup(
    name='prikoloff_theme',
    version='1.5.6',
    author='Oksana Zaporozhets',
    author_email='zap.oksana@gmail.com',
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    license='Creative Commons Attribution-Noncommercial-Share Alike license',
    long_description=open('README.txt').read(),
    entry_points={
        'sshop.plugins.settings.entrypoints': [
            'update_settings = prikoloff_theme:update_settings',
            'install = prikoloff_theme:install',
            'upgrade = prikoloff_theme:upgrade',
        ],
    },  
)
